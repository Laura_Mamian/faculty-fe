import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue';

import SignIn from './components/SignIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'

const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/user/signIn',
        name: "signIn",
        component: SignIn
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/user/home',
        name: "home",
        component: Home
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
